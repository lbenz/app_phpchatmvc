<?php

/**
 * Initialize necessary configurations and constants
 * of application.
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

setlocale(LC_TIME, 'fra', 'fr_FR');
date_default_timezone_set('Europe/Paris');

define('HOST', 'http://php-chat-mvc.local/web/app.php');
define('DS', DIRECTORY_SEPARATOR);
define('DIR_ROOT', __DIR__ . DS . '..' . DS);
define('DIR_CACHE', DIR_ROOT . 'var/cache' . DS);
define('DIR_SRC', DIR_ROOT . 'src' . DS);
define('DIR_CONFIG', DIR_ROOT . 'config' . DS);
define('DIR_RESOURCES', DIR_SRC . 'Resources' . DS);
define('DIR_VIEWS', DIR_RESOURCES . 'views' . DS);
define('DIR_SESSIONS', DIR_ROOT . 'var' . DS . 'sessions' . DS);
define('NAMESPACE_CONTROLLER', 'App\\Controllers\\');
define('DATABASE_HOST', 'mysql');
define('DATABASE_PORT', '3306');
define('DATABASE_NAME', 'php-chat-mvc');
define('DATABASE_USER', 'php-chat-mvc');
define('DATABASE_PASSWORD', 'php-chat-mvc');
define('SECRET', '6F66CEFB842926F5A2C1817278CBE');
define('ROLE_ALL', '3');
define('ROLE_ANONYMOUS', '1');
define('ROLE_CONNECTED', '2');
