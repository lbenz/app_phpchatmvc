<?php

/**
 * Boostrap class provide main method
 * to boot application kernel.
 */

namespace App\Config;

use App\Router\Router;

class Bootstrap {

    /**
     * Boot the application
     *
     * @throws \Exception
     */
    final public static function bootApp() {
        self::launchBeforeBoot();
        Autoloader::register();
        Router::launch();
        self::launchAfterBoot();
    }

    /**
     * Set extra config before boot the application.
     *
     * @author Lounis BENAMAR
     */
    final private static function launchBeforeBoot() {
        // stuff here
        session_save_path(DIR_SESSIONS); // set new session folder
        session_start(); // start session
    }

    /**
     * Set extra config before boot the application.
     *
     * @author Lounis BENAMAR
     */
    final private static function launchAfterBoot() {
        // stuff here
    }
}