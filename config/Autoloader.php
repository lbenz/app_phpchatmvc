<?php

/**
 * Autoloader class.
 * No dependency here, only provide the
 * way to retrieve PHP Class the application need.
 */

namespace App\Config;

final class Autoloader {

    /**
     * Static method to call from bootstrap file.
     */
    final public static function register() {
        spl_autoload_register(array(__CLASS__, 'loader'));
    }

    /** Set loader to automatically load files(only once)
     * TODO: find a way to don't face issue if some end's namespace are similar
     *
     * @param $className
     * @return bool
     * @author Lounis BENAMAR
     */
    final private static function loader($className) {
        $phpFileList = self::getFilesRecursively(DIR_SRC, ['.php']);
        foreach($phpFileList as $file) {
            $formatedClassName = implode('/', (array_slice(explode('\\', $className), -2, 2)));
            $formatedFile = str_replace('.php', '', implode('/', (array_slice(explode('/', $file), -2, 2))));
            // if file corresponding
            if(strcasecmp($formatedFile, $formatedClassName) === 0) {
                // see if the file exists
                if(file_exists($file)) {
                    require_once($file);
                    if(class_exists($className)) {
                        return true;
                    }
                }
            } else {
                continue;
            }
        }
    }

    /**
     * Obtain files recursively from base folder,
     * and check extension or origin.
     *
     * @param $basedir
     * @param $extensionsAllowed
     * @return array
     * @author Lounis BENAMAR
     */
    final private static function getFilesRecursively($basedir, $extensionsAllowed) {
        $result = array();
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($basedir)
        );
        foreach($files as $fileObject) {
            $fileName = $fileObject->getFileName();
            $filePath = $fileObject->getPathName();
            $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);
            $isFromResources = stripos($filePath, '/resource');
            if(!$fileExt || $isFromResources) {
                continue;
            }
            foreach($extensionsAllowed as $allowedExt) {
                if(strpos($allowedExt, $fileExt)) {
                    $result[] = $filePath;
                    break;
                }
            }
        }

        return $result;
    }
}
