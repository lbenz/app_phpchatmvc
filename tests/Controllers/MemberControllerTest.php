<?php

namespace App\Tests\Controllers;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class MemberControllerTest extends TestCase{

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testLoginAction() {
        $url = 'http://localhost/web/app.php?login'; // host on docker phpapache container - TODO: need to configure docker to change host of container
        $client = new Client(['allow_redirects' => true]);
        $response = $client->request('POST', $url, [
            'form_params' => [
                'username' => 'yuri',
                'password' => 'test'
            ],
            'allow_redirects' => true,
        ]);

        // TODO assert other statement (redirect, session, member, ...)
        $this->assertEquals(200, $response->getStatusCode());
    }

}