var send = $(function() {
    var $message = $('#message');
    var $comment = $('form[name="formSend"]').find('#comment');
    $('#speak').click(function() {
        $.ajax({
            url: 'box/message/send',
            type: 'post',
            data: $('form[name="formSend"]').serialize(),
            dataType: 'json',
            success : function(data){
                $comment.val(''); // empty textarea
                $message.append(
                    "<p class='line' id=" + data['message_id'] + ">" +
                        "<span>"+ timestampToDateFr(data['message_timestamp']) +" - </span>" +
                        "<span>"+ data['member_username'] +" : </span>" +
                        "<span>"+ data['message_text'] +"</span>" +
                    "</p>"
                );
            },
            error : function(error){
                console.log(error);
            }
        });
    });
});
