var box = $(function() {
    var $message = $('#message');
    var refreshBox = setInterval(function() {
        $.ajax({
            url: '/box/message/last',
            type: 'get',
            dataType: 'json',
            success : function(data){
                if($message.find('#' + data['message_id']).length === 0) {
                    $message.append(
                        "<p class='line' id=" + data['message_id'] + ">" +
                        "<span>"+ timestampToDateFr(data['message_timestamp']) +" - </span>" +
                        "<span>"+ data['member_username'] +" : </span>" +
                        "<span>"+ data['message_text'] +"</span>" +
                        "</p>"
                    );
                    $message.scrollTop($message[0].scrollHeight);
                }
            },
            error : function(error){
                console.log(error);
            }
        });
    }, 1000);
});

var listUser = $(function() {
    var $listUser = $('#list-user');
    var refreshListUser = setInterval(function() {
        $.ajax({
            url: '/box/users/connected',
            type: 'get',
            dataType: 'json',
            success : function(data){
                $listUser.empty();
                var listUserContent = '<span>Users connected : [</span>';
                for(var user in data) {
                    var info = data[user];
                    listUserContent += '<span title="Last activity '+ timestampToDateFr(info['lastActivity']) +  '"><a href="#"> '+ info['username'] +'</a> </span>';
                }
                listUserContent += '<span>]</span>';
                $listUser.append($(listUserContent));
            },
            error : function(error){
                console.log(error);
            }
        });
    }, 15000);
});
