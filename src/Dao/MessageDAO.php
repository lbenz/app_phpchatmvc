<?php

/**
 * Message DAO.
 */

namespace App\Dao;

/**
 * Class MessageDAO
 * @package App\Dao
 */
class MessageDAO extends AbstractDAO {

    /**
     * @param $params
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function create($params)
    {
        $s = $this->query(
            "INSERT INTO {$this->getSQLTable()}
                      (`timestamp`, text)
                       VALUES
                      (:timestamp, :text)",
            $params
        );

        if($s->execute()) {
            return $this->getById($this->connection->lastInsertId());
        }

        return false;
    }

    /**
     * @param $id
     * @param $params
     * @return mixed|void
     */
    public function updateById($id, $params) {
        // TODO: Implement updateById() method.
    }
}