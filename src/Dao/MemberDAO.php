<?php

/**
 * Member DAO.
 */

namespace App\Dao;

/**
 * Class MemberDAO
 * @package App\Dao
 */
class MemberDAO extends AbstractDAO {

    /**
     * Retrieve member by login
     *
     * @param $username
     * @param $password
     * @return mixed|null
     * @throws \Exception
     */
    public function getByLogin($username, $password) {
        $s = $this->query(
            "SELECT id, username, mail from {$this->getSQLTable()}
                       WHERE username = :username
                       AND password = :password",
            ['username' => $username, 'password' => $password]
        );
        $s->setFetchMode(\PDO::FETCH_CLASS, $this->getCurrentModel());

        if($s->execute()) {
            return $s->fetch() ?? null;
        }

        return null;
    }

    /**
     * Create member
     *
     * @param $params
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function create($params) {
        $s = $this->query(
            "INSERT INTO {$this->getSQLTable()}
                      (username, password, mail)
                       VALUES
                      (:username, :password, :mail)",
            $params
        );

        if($s->execute()) {
            return $this->getById($this->connection->lastInsertId());
        }

        return false;
    }

    /**
     * Update member
     *
     * @param $id
     * @param $params
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function updateById($id, $params)
    {
        $s = $this->query(
            "UPDATE {$this->getSQLTable()}
                  SET username = :username,
                  password = :password,
                  mail = :mail
                  WHERE id = :id",
            $params
        );

        if($s->execute()) {
            return $this->getById($this->connection->lastInsertId());
        }

        return false;
    }
}