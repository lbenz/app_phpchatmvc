<?php

/**
 * Fill methods provide by DAO contract.
 */

namespace App\Dao;

use App\Interfaces\DataAccessObjectInterface;
use App\Models\Member;
use App\Models\Message;
use App\Models\MessageMember;
use App\Services\DatabaseConnection;

/**
 * Class AbstractDAO
 * @package App\Dao
 */
abstract class AbstractDAO implements DataAccessObjectInterface
{

    /**
     * @var \PDO
     */
    protected $connection;

    /**
     * AbstractDAO constructor.
     */
    public function __construct() {
        $this->connection = DatabaseConnection::getConnection();
    }

    /**
     * Return SQL table name associates to Entity linked to DAO
     *
     * @return string
     */
    final protected function getSQLTable()
    {
        $calledClass = get_called_class();
        $logicName = substr($calledClass, strrpos($calledClass, '\\') + 1);
        return strtolower(str_replace('DAO', '', $logicName));
    }

    /**
     * Get current model namespace to use \PDO convert result to object
     *
     * @return string
     * @throws \Exception
     */
    final protected function getCurrentModel() {
        $models = array(
            new Member(),
            new Message(),
            new MessageMember(),
        );

        foreach($models as $model) {
            $modelNamespace = get_class($model);
            $modelName = substr($modelNamespace, strrpos($modelNamespace, '\\') + 1);

            if(strcasecmp($modelName, $this->getSQLTable()) === 0) {
                return $modelNamespace;
            }
        }

        throw new \Exception('Mapping error');
    }

    /**
     * Get PDO type from PHP variable type hint
     *
     * @param $bindParameter
     * @return int
     */
    final protected function getPDOType($bindParameter) {
        $type = gettype($bindParameter);
        switch($type) {
            case 'boolean':
                return \PDO::PARAM_BOOL;
            case 'integer':
                return \PDO::PARAM_INT;
            case 'null':
                return \PDO::PARAM_NULL;
            case 'double':
            case 'string':
            default:
                return \PDO::PARAM_STR;
        }
    }

    /**
     * Start SQL transaction
     *
     * @return bool
     */
    public function beginTransaction() {
        return $this->connection->beginTransaction();
    }

    /**
     * Commit SQL transaction
     *
     * @return bool
     */
    public function commit() {
        return $this->connection->commit();
    }

    /**
     * Rollback SQL transaction
     *
     * @return bool
     */
    public function rollback() {
        return $this->connection->rollBack();
    }

    /**
     * Check if statement is in transaction
     *
     * @return bool
     */
    public function inTransaction() {
        return $this->connection->inTransaction();
    }

    /**
     * Prepare PDO query
     *
     * @param $query
     * @param null $bindParameters
     * @return bool|\PDOStatement
     */
    public function query($query, $bindParameters = null) {
        $s = $this->connection->prepare($query);

        if(is_array($bindParameters) && !empty($bindParameters)) {
            foreach($bindParameters as $field => &$value) {
                $param = ':' . $field;
                $typePDO = $this->getPDOType($value);
                $s->bindParam($param, $value, $typePDO);
            }
        }

        return $s;
    }

    /**
     * Count all record of table
     *
     * @return mixed
     */
    public function countAll() {
        $s = $this->query("SELECT COUNT(*) as `count` FROM {$this->getSQLTable()}");
        if($s->execute()) {
            return $s->fetch()['count'] ?? null;
        }

        return null;
    }

    /**
     * Get all records
     *
     * @return array|mixed
     * @throws \Exception
     */
    public function getAll() {
        $s = $this->query("SELECT * FROM {$this->getSQLTable()}");
        if($s->execute()) {
            return $s->fetchAll(\PDO::FETCH_CLASS, $this->getCurrentModel()) ?? [];
        }


        return [];
    }

    /**
     * Get record by id
     *
     * @param $id
     * @return mixed|null
     * @throws \Exception
     */
    public function getById($id) {
        $s = $this->query(
            "SELECT * FROM {$this->getSQLTable()}
                   WHERE id = :id",
            ['id' => $id]
        );
        $s->setFetchMode(\PDO::FETCH_CLASS, $this->getCurrentModel());
        if($s->execute()) {
            return $s->fetch() ?? null;
        }

        return null;
    }

    /**
     * Delete specific record
     *
     * @param $id
     * @return bool
     */
    public function deleteById($id) {
        $s = $this->query(
            "DELETE FROM {$this->getSQLTable()}
                   WHERE id = :id",
            ['id' => $id]
        );

        return $s->execute();
    }

    /**
     * Update specific record
     *
     * @param $id
     * @param $params
     */
    abstract function updateById($id, $params);

    /**
     * Create specific record
     *
     * @param $params
     * @return mixed
     */
    abstract public function create($params);

}