<?php

/**
 * MessageMember DAO
 */

namespace App\Dao;

use App\Models\MessageMember;

/**
 * Class MessageMemberDAO
 * @package App\Dao
 */
class MessageMemberDAO extends AbstractDAO {

    /**
     * MessageMemberDAO constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Create new MessageMember record
     *
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    public function create($params)
    {
        $s = $this->query(
            "INSERT INTO {$this->getSQLTable()}
                      (member_id, message_id)
                       VALUES
                      (:member_id, :message_id)",
            $params
        );

        if($s->execute()) {
            return $this->getByParams($params);
        }

        return null;
    }

    /**
     * Get messageMember by its two ids
     *
     * @param $params
     * @return MessageMember|null
     * @throws \Exception
     */
    public function getByParams($params) {
        $s = $this->query(
            "SELECT * FROM {$this->getSQLTable()} AS mm
                   INNER JOIN message as m ON mm.message_id = m.id
                   INNER JOIN member as mb ON mm.member_id = mb.id
                   WHERE member_id = :member_id
                   AND message_id = :message_id",
            $params
        );

        if($s->execute()) {
            $record = $s->fetch(\PDO::FETCH_ASSOC);
            if(!empty($record)) {
                return (new MessageMember())
                    ->setMember((new MemberDAO())->getById($record['member_id']))
                    ->setMessage((new MessageDAO())->getById($record['message_id']));
            }
        }

        return null;
    }

    /**
     * Get latest records at specific range
     *
     * @param $limit
     * @return array|null
     * @throws \Exception
     */
    public function getLastRange($limit) {
        $s = $this->query(
            "SELECT mm.member_id, mm.message_id FROM {$this->getSQLTable()} AS mm
                   INNER JOIN message as m ON mm.message_id = m.id
                   INNER JOIN member as mb ON mm.member_id = mb.id
                   ORDER BY CAST(m.timestamp as unsigned) ASC LIMIT :limit",
            ['limit' => $limit]
        );

        if($s->execute()) {
            $records = $s->fetchAll(\PDO::FETCH_ASSOC);
            if(!empty($records)) {
                foreach($records as $key => $record) {
                    $records[$key] = (new MessageMember())
                        ->setMember((new MemberDAO())->getById($record['member_id']))
                        ->setMessage((new MessageDAO())->getById($record['message_id']));
                }

                return $records;
            }
        }

        return [];
    }

    /**
     * Get last record
     *
     * @return MessageMember|null
     * @throws \Exception
     */
    public function getLastRecord() {
        $s = $this->query(
            "SELECT * FROM {$this->getSQLTable()}
                   ORDER BY message_id DESC LIMIT 1"
        );

        if($s->execute()) {
            $record = $s->fetch(\PDO::FETCH_ASSOC);
            if(!empty($record)) {
                return (new MessageMember())
                    ->setMember((new MemberDAO())->getById($record['member_id']))
                    ->setMessage((new MessageDAO())->getById($record['message_id']));
            }
        }

        return null;
    }

    /**
     * Get all records where timestamp is inferior than yesterday exactly (hour:min:sec)
     *
     * @return array
     * @throws \Exception
     */
    public function getAllMessagesBeforeToday() {
        $today = time();
        $yesterday = strtotime('-1 day', $today);
        $s = $this->query(
            "SELECT * FROM {$this->getSQLTable()} as mm
                   INNER JOIN message as m
                   ON mm.message_id = m.id
                   WHERE m.timestamp < :yesterday
                   ORDER BY CAST(m.timestamp as unsigned) ASC",
            ['yesterday' => $yesterday]
        );

        if($s->execute()) {
            $records = $s->fetchAll(\PDO::FETCH_ASSOC);
            if(!empty($records)) {
                foreach($records as $key => $record) {
                    $records[$key] = (new MessageMember())
                        ->setMember((new MemberDAO())->getById($record['member_id']))
                        ->setMessage((new MessageDAO())->getById($record['message_id']));
                }

                return $records;
            }
        }

        return [];
    }

    /**
     * @param $id
     * @param $params
     */
    public function updateById($id, $params) {
        // TODO: Implement updateById() method.
    }
}