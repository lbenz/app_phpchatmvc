<?php

/**
 * Router start routing of application
 * and retrieve the right process for the statement.
 */

namespace App\Router;

use App\Controllers\ErrorController;

/**
 * Class Router
 * @package App\Router
 */
class Router {

    /**
     * Handle all routes of application,
     * This handler is configured to work only with QueryString route.
     */
    final public static function launch() {

        Sanitizer::sanitizeInput();
        $routing = Handler::getRouting();
        $routeRequest = Checker::checkRouteMatch($routing);

        if($routeRequest instanceof RouteRequest) {
            $route = $routeRequest->route;
            $controller = NAMESPACE_CONTROLLER . $route['controller'];
            $action = $route['action'];
            $role = $route['role'];
            $methods = $route['methods'];
            Checker::checkMethods($methods);
            Checker::checkRole($role);
            try {
                Checker::checkControllerAction($controller, $action);
                Displayer::displayResponse(new $controller, $action, $routeRequest); // call controller and action method.
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }

        Displayer::displayResponse(new ErrorController(), 'error404Action');
    }

}