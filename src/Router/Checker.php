<?php

/**
 * Checker class provides methods to check
 * at different levels data and access.
 */

namespace App\Router;

use App\Services\SessionManager;

/**
 * Class Checker
 * @package App\Router
 */
final class Checker {

    /**
     * Check role of user, anonymous go back to login page,
     * connected go to box page
     *
     * @param $routeRole
     * TODO: need to finish this method (may presents some issue) !
     */
    final public static function checkRole($routeRole) {
        $member = SessionManager::getSession('member');
        if($routeRole === ROLE_ALL) {
            // TODO: reflection on further statement here
        } else if($routeRole === ROLE_CONNECTED) {
            if($member['role'] < ROLE_CONNECTED) {
                header('Location: /login'); // login page
            } else {
                // user has some activity, so refresh is timestamp
                $member['timestamp'] = time();
                SessionManager::setSession('member', $member);
            }
        } else if($routeRole === ROLE_ANONYMOUS) {
            if($member['role'] > ROLE_ANONYMOUS) {
                header('Location: /box'); // box page
            }
        }
    }

    /**
     * Check conformity of Controller and action call.
     *
     * @param $controller
     * @param $action
     * @throws \Exception
     */
    final public static function checkControllerAction($controller, $action) {
        if (!class_exists($controller)) {
            throw new \Exception('Controller not found : ' . $controller);
        }
        if (!method_exists(new $controller, $action)) {
            throw new \Exception('Action not found : ' . $action . ' from Controller : ' . $controller);
        }
    }

    /**
     * Check if method used is allowed
     *
     * @param $routeMethods
     * @return bool
     * @throws \Exception
     */
    final public static function checkMethods($routeMethods) {
        $currentMethod = $_SERVER['REQUEST_METHOD'];
        if(array_key_exists($currentMethod, array_flip($routeMethods)) === true) {
            return true;
        }

        throw new \Exception('Method not allowed (405 Error)');
    }

    /**
     * Check route pattern,
     * get route parameters and route information.
     * Return null if no match is found, otherwise we obtain RouteRequest instance.
     *
     * @param $routing
     * @return RouteRequest|null
     */
    final public static function checkRouteMatch($routing) {

        $skip = false; // allow to skip some iteration (performance issue)

        foreach($routing as $pattern => $route) {
            $patternAsArray = array_filter(explode('/', $pattern));
            $uriAsArray = array_filter(explode('/', $_SERVER['REDIRECT_URL'] ?? '/'));
            $routeParams = [];

            foreach($patternAsArray as $k => $part) {
                if(strpos($part, '{') !== false && strpos($part, '}')) {
                    if(isset($requestUriAsArray[$k])) {
                        $routeParams[str_replace(['{', '}'], '' , $part)] = $uriAsArray[$k];
                        $patternAsArray[$k] = $uriAsArray[$k];
                    } else {
                        $skip = true;
                        break;
                    }
                }
            }

            if($skip) {
                continue;
            }

            if(implode('/', $patternAsArray) === implode('/', $uriAsArray)) {
                return new RouteRequest(
                    $routeParams,
                    $route
                );
            }
        }

        return null;
    }

}