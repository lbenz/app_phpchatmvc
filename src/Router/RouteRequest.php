<?php

/**
 * RouteRequest contains many data
 * about requests send to application.
 */

namespace App\Router;

/**
 * Class RouteRequest
 * @package App\Router
 */
class RouteRequest {

    /**
     * @var array
     */
    public $routeParams;

    /**
     * @var array
     */
    public $route;

    /**
     * @var string
     */
    public $uri;

    /**
     * @var array
     */
    public $post;

    /**
     * @var array
     */
    public $get;

    /**
     * @var array
     */
    public $files;

    /**
     * RouteRequest constructor.
     *
     * @param $routeParams
     * @param $route
     */
    public function __construct($routeParams, $route) {
        $this->routeParams = $routeParams;
        $this->route = $route;
        $this->uri = $_SERVER['REDIRECT_URL'] ?? '/';
        $this->post = $_POST;
        $this->get = $_GET;
        $this->files = $_FILES;
    }

}