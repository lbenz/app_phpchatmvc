<?php

/**
 * Handler contains all routes of application.
 */

namespace App\Router;

/**
 * Class Handler
 * @package App\Router
 */
final class Handler {

    /**
     * Configure routing
     *
     * @return array
     */
    final public static function getRouting() {
        $routing = array(
            '/' => [
                'controller' => 'IndexController',
                'action' => 'indexAction',
                'methods' => ['GET'],
                'role' => ROLE_ALL
            ],

            // Member

            '/login' => [
                'controller' => 'MemberController',
                'action' => 'loginAction',
                'methods' => ['GET', 'POST'],
                'role' => ROLE_ANONYMOUS
            ],

            '/logout' => [
                'controller' => 'MemberController',
                'action' => 'logoutAction',
                'methods' => ['GET'],
                'role' => ROLE_CONNECTED
            ],

            '/register' => [
                'controller' => 'RegisterController',
                'action' => 'registerAction',
                'methods' => ['GET', 'POST'],
                'role' => ROLE_ANONYMOUS
            ],

            '/generate' => [
                'controller' => 'MemberController',
                'action' => 'generateAction',
                'methods' => ['GET'],
                'role' => ROLE_ALL
            ],

            // Box

            '/box' => [
                'controller' => 'BoxController',
                'action' => 'boxAction',
                'methods' => ['GET'],
                'role' => ROLE_CONNECTED
            ],

            '/box/message/last' => [
                'controller' => 'BoxController',
                'action' => 'getLastMessageBoxAction',
                'methods' => ['GET'],
                'role' => ROLE_CONNECTED
            ],

            '/box/users/connected' => [
                'controller' => 'BoxController',
                'action' => 'getBoxUsersConnectedAction',
                'methods' => ['GET'],
                'role' => ROLE_CONNECTED
            ],

            '/box/message/send' => [
                'controller' => 'BoxController',
                'action' => 'sendAction',
                'methods' => ['POST'],
                'role' => ROLE_CONNECTED
            ],

            '/box/message/archives' => [
                'controller' => 'BoxController',
                'action' => 'archivesAction',
                'methods' => ['GET'],
                'role' => ROLE_CONNECTED
            ],

            // Error

            '/error-404' => [
                'controller' => 'ErrorController',
                'action' => 'error404Action',
                'methods' => ['GET'],
                'role' => ROLE_ALL
            ],
        );
        ksort($routing);

        return $routing;
    }
}