<?php

/**
 * Sanitizer provides method to clean
 * application input data.
 */

namespace App\Router;

/**
 * Class Sanitizer
 * @package App\Router
 */
final class Sanitizer {

    /**
     * Sanitize POST and GET entries to prevent dirty hack
     *
     * @author Lounis BENAMAR
     */
    final public static function sanitizeInput() {
        $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Crypt all fields contains 'password' pattern in POST
        if(!empty($_POST)) {
            foreach($_POST as $k => $v) {
                if(stripos($k, 'password') !== false) {
                    $_POST[$k] = sha1($v);
                }
            }
        }
    }

}