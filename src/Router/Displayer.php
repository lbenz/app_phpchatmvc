<?php

/**
 * Displayer sends output to application.
 */

namespace App\Router;

/**
 * Class Displayer
 * @package App\Router
 */
final class Displayer {

    /**
     * Display response
     *
     * @param $controller
     * @param $action
     * @param $routeRequest
     */
    final public static function displayResponse($controller, $action, $routeRequest = null) {
        $response = call_user_func_array([$controller, $action], ['routeRequest' => $routeRequest]);
        echo $response;
        exit();
    }

}