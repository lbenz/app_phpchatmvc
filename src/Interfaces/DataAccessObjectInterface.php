<?php

/**
 * Contract to use Data Access Object.
 */

namespace App\Interfaces;

/**
 * Interface DataAccessObjectInterface
 * @package App\Interfaces
 */
interface DataAccessObjectInterface {

    /**
     * @return mixed
     */
    public function beginTransaction();

    /**
     * @return mixed
     */
    public function commit();

    /**
     * @return mixed
     */
    public function rollback();

    /**
     * @return mixed
     */
    public function inTransaction();

    /**
     * @param $query
     * @param null $bindParameters
     * @return mixed
     */
    public function query($query, $bindParameters = null);

    /**
     * @return mixed
     */
    public function countAll();

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
     public function updateById($id, $params);

    /**
     * @param $params
     * @return mixed
     */
     public function create($params);
}