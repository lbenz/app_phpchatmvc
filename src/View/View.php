<?php

/**
 * View serves old way to treat application templating.
 */

namespace App\View;

use App\Services\SessionManager;

/**
 * Class View
 * @package App\View
 * @deprecated
 */
class View {

    /**
     * Sanitize data provided
     *
     * @param $data
     * @return string
     */
    public static function sanitize($data) {
        return htmlspecialchars((string) $data, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Render template, returning it's content
     * with extract data $data.
     *
     * @param $data
     * @return false|string
     * @throws \Exception
     */
    public static function render($data) {
        $template = DIR_RESOURCES . 'default.phtml';
        // prevent existing variable - forbidden to rewrite them
        if(isset($data['view']) || isset($data['container'])) {
            throw new \Exception('Forbidden to write view or container in array argument of ' . __CLASS__ . '::render($params)');
        }
        if(!isset($data['provider'])) {
            throw new \Exception('Provider data missing to ' . __CLASS__ . '::render($params)');
        }

        $data['view'] = new View();
        $data['container'] = new SessionManager();
        extract($data);
        ob_start();
        include_once($template);
        $content = ob_get_clean();
        return $content;
    }

    /**
     * Include view from views folder at Resources
     * TODO: find a way to get external data from this method
     *
     * @param $viewPath
     * @return mixed
     */
    public static function addTemplate($viewPath) {
        include(DIR_VIEWS . $viewPath);
    }

}