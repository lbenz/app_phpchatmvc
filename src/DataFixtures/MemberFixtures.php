<?php

namespace App\DataFixtures;

use App\Dao\MemberDAO;

class MemberFixtures {

    /**
     * Generate members
     * @throws \Exception
     */
    public static function generateMember() {
        $memberRepo = new MemberDAO();
        $members = array(
            array('username' => 'jacky', 'password' => sha1('test'), 'mail' => 'jacky@jacky.fr'),
            array('username' => 'boletta', 'password' => sha1('test'), 'mail' => 'boletta@boletta.fr'),
            array('username' => 'yuri', 'password' => sha1('test'), 'mail' => 'yuri@yuri.fr'),
            array('username' => 'fredodo', 'password' => sha1('test'), 'mail' => 'fredodo@fredodo.fr'),
            array('username' => 'batry94', 'password' => sha1('test'), 'mail' => 'batry94@batry94.fr'),
        );
        $result = [];
        foreach($members as $member) {
            $result[] = $memberRepo->create($member);
        }

        if(count($result) === count($members)) {
            var_dump($result);
            var_dump('password = test');
        } else {
            throw new \Exception("An error occurred while generating members");
        }
    }

}