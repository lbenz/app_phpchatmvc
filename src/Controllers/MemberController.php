<?php

namespace App\Controllers;

use App\DataFixtures\MemberFixtures;
use App\Services\SessionManager;
use App\View\View;
use App\Dao\MemberDAO;

class MemberController extends AbstractBaseController {

    /**
     * MemberController constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->repo = new MemberDAO();
    }

    /**
     * Login user
     * @param null $params
     * @return false|string
     * @throws \Exception
     */
    public function loginAction($params = null) {

        if(isset($_POST)) {
            $member = $this->repo->getByLogin($_POST['username'], $_POST['password']);
            if($member) {
                SessionManager::setSession('member', [
                    'id' => $member->getId(),
                    'username' => $member->getUsername(),
                    'connected' => true,
                    'timestamp' => time(),
                    'role' => ROLE_CONNECTED,
                    'serialize' => serialize($member),
                ]);

                header('Location: /box');
            }
        }

        return $this->render('Login/login.html.twig', []);
    }

    /**
     * Logout user
     */
    public function logoutAction() {
        session_unset();
        session_destroy();
        header('Location: /');
    }

    /**
     * Display result of generate member fixtures
     * @throws \Exception
     */
    public function generateAction() {
        MemberFixtures::generateMember();
    }
}