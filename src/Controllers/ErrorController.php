<?php

namespace App\Controllers;

use App\View\View;

class ErrorController extends AbstractBaseController {

    /**
     * @return false|string
     * @throws \Exception
     */
    public function error404Action() {
        http_response_code(404);
        return View::render(['provider' => $this->getControllerLogicName()]);
    }

}