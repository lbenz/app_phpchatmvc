<?php

/**
 * AbstractBaseController facilitates
 * handling of Controller.
 */

namespace App\Controllers;

use App\Services\SessionManager;
use App\Utils\Core\FileUtils;

/**
 * Class AbstractBaseController
 * @package App\Controllers
 */
abstract class AbstractBaseController {

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var mixed
     */
    protected $repo;

    /**
     * AbstractBaseController constructor.
     */
    public function __construct() {
        $this->twig = new \Twig_Environment(
            new \Twig_Loader_Filesystem(
                FileUtils::getDirsRecursively(DIR_VIEWS),
                DIR_ROOT)
//            ['cache' => DIR_CACHE]
        );
    }

    /**
     * Get controller logic name (Example Login, Register)
     * @return mixed
     */
    protected function getControllerLogicName() {
        $calledClass = get_called_class();
        $logicName = substr($calledClass, strrpos($calledClass, '\\') + 1);
        return strtolower(str_replace('Controller', '', $logicName));
    }

    /**
     * @return mixed
     */
    protected function getUser() {
        return SessionManager::getSession('member');
    }

    /**
     * This method is an alias for Twig_Environment::render
     *
     * @param string $template
     * @param array $context
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function render(string $template, array $context) {
        return $this->twig->render($template, $context);
    }

}