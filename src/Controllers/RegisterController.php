<?php

namespace App\Controllers;

Use App\View\View;
use App\Dao\MemberDAO;
use App\Services\SessionManager;

class RegisterController extends AbstractBaseController {

    public function registerAction() {

        if(isset($_POST)) {
            $membersRepo = new MemberDAO();
            $member = $membersRepo->create($_POST);

            if($member) {
                SessionManager::setSession('member', [
                    'id' => $member->getId(),
                    'username' => $member->getUsername(),
                    'connected' => true,
                    'timestamp' => time(),
                    'role' => ROLE_CONNECTED,
                    'serialize' => serialize($member),
                ]);

                header('Location: /box');
            }
        }

        return View::render(['provider' => $this->getControllerLogicName()]);
    }
}