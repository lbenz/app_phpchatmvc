<?php

namespace App\Controllers;

class IndexController extends AbstractBaseController {

    /**
     * @return false|string
     * @throws \Exception
     */
    public function indexAction() {
        return $this->render('Index/index.html.twig', []);
    }

}