<?php

namespace App\Controllers;

use App\Dao\MessageDAO;
use App\Dao\MessageMemberDAO;
use App\Services\SessionManager;
use App\Utils\MemberUtils;
use App\View\View;

class BoxController extends AbstractBaseController {

    public function __construct() {
        $this->repo = new MessageMemberDAO();
    }

    /**
     * Display tchat box with last 20 messages
     *
     * @param null $params
     * @return false|string
     * @throws \Exception
     */
    public function boxAction($params = null) {
        $messageMemberList = $this->repo->getLastRange(20);
        $membersConnected = MemberUtils::getAllUsersConnected();

        return View::render([
            'provider' => $this->getControllerLogicName(),
            'messageMemberList' => $messageMemberList,
            'membersConnected' => $membersConnected,
        ]);
    }

    /**
     * Get all users connected in TchatBox
     */
    public function getBoxUsersConnectedAction() {
        echo json_encode(MemberUtils::getAllUsersConnected());
    }

    /**
     * Get last message created
     *
     * @param null $params
     * @throws \Exception
     */
    public function getLastMessageBoxAction($params = null) {
        $messageMemberLast = $this->repo->getLastRecord();

        echo json_encode(array(
            'member_username' => $messageMemberLast->getMember()->getUsername(),
            'message_timestamp' => $messageMemberLast->getMessage()->getTimestamp(),
            'message_id' => $messageMemberLast->getMessage()->getId(),
            'message_text' => $messageMemberLast->getMessage()->getText(),
        ));
    }

    /**
     * Send message action
     *
     * @param null $params
     * @throws \Exception
     */
    public function sendAction($params = null) {
        if(isset($_POST['comment'])) {
            $params = array(
                'text' => $_POST['comment'],
                'timestamp' => time(),
            );
            $messageRepo = new MessageDAO();
            
            $message = $messageRepo->create($params);
            $messageMember = $this->repo->create([
                'member_id' => SessionManager::getSession('member')['id'],
                'message_id' => $message->getId(),
            ]);

            echo json_encode(array(
                'member_username' => $messageMember->getMember()->getUsername(),
                'message_timestamp' => $messageMember->getMessage()->getTimestamp(),
                'message_id' => $messageMember->getMessage()->getId(),
                'message_text' => $messageMember->getMessage()->getText(),
            ));
        }
    }

    /**
     * @param null $params
     * @return false|string
     * @throws \Exception
     */
    public function archivesAction($params = null) {
        $messageMemberList = $this->repo->getAllMessagesBeforeToday();

        return View::render([
            'provider' => $this->getControllerLogicName(),
            'messageMemberList' => $messageMemberList,
            'archives' => true,
        ]);
    }
}