<?php

/**
 * Manage SQL database connectivity
 */

namespace App\Services;

/**
 * Class DatabaseConnection
 * @package App\Services
 * @author Lounis BENAMAR
 */
class DatabaseConnection {

    /**
     * @var null
     */
    private static $instance = null;

    /**
     * @var \PDO
     */
    private $connection;

    /**
     * DatabaseConnection constructor.
     */
    private function __construct() {
        try {
            $this->connection = new \PDO(
                "mysql:host=" . DATABASE_HOST . ";
                dbname=" . DATABASE_NAME,
                DATABASE_USER,
                DATABASE_PASSWORD,
                array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                )
            );

        } catch(\PDOException $e) {
            var_dump($e->getMessage());
        }
    }

    /**
     * Get unique instance of DatabaseConnection
     *
     * @return null|DatabaseConnection
     * @author Lounis BENAMAR
     */
    private static function getInstance() {
        if(!self::$instance) {
            $class = __CLASS__;
            self::$instance = new $class;
        }

        return self::$instance;
    }

    /**
     * Get Database connection. Best practice would be to use this only in DAO class
     *
     * @return \PDO
     * @author Lounis BENAMAR
     */
    public static function getConnection() {
        return self::getInstance()->connection;
    }

    /**
     * Close connection
     *
     * @author Lounis BENAMAR
     */
    public static function closeConnection() {
        self::getInstance()->connection = null;
    }

}