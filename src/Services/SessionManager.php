<?php

/**
 * Session manager grant an easier access
 * to PHP Sessions.
 */

namespace App\Services;

/**
 * Class SessionManager
 * @package App\Services
 */
final class SessionManager {

    /**
     * Get session by key
     * @param $key
     * @return null
     */
    public static function getSession($key) {
        if(isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return null;
    }

    /**
     * Set session with key and data
     * @param $key
     * @param $data
     * @return bool
     */
    public static function setSession($key, $data) {
        $_SESSION[$key] = $data;
        if(isset($_SESSION[$key])) {
            if($_SESSION[$key] === $data) {
                return true;
            }
        }

        return false;
    }

    /**
     * Delete session by key
     * @param $key
     * @return bool
     */
    public static function deleteSession($key) {
        unset($_SESSION[$key]);
        if(!isset($_SESSION[$key])) {
            return true;
        }

        return false;
    }

    /**
     * Erase all session data
     * @return bool
     */
    public static function closeSession() {
        session_unset();
        session_destroy();
        session_abort();
        if(empty($_SESSION)) {
            return true;
        }

        return false;
    }
}