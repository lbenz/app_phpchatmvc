<?php

namespace App\Models;

class MessageMember {

    /**
     * @var Member
     */
    private $member;

    /**
     * @var Message
     */
    private $message;

    /**
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * @param $member
     * @return $this
     */
    public function setMember($member)
    {
        $this->member = $member;
        return $this;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Create MessageMember object from an array of data
     *
     * @param $data
     * @return MessageMember
     */
    public function arrayToObject($data) {
        return (new MessageMember())
            ->setMember($data['member'])
            ->setMessage($data['message']);
    }

}