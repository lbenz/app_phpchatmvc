<?php

/**
 * FileUtils provide methods
 * for file manipulation.
 */

namespace App\Utils\Core;

final class FileUtils {

    /**
     * Obtain files recursively from base folder,
     * and check extension or origin.
     *
     * @param $basedir
     * @return \RecursiveIteratorIterator
     * @author Lounis BENAMAR
     */
    final public static function getTree($basedir) {
        $tree = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($basedir)
        );

        return $tree;
    }

    /**
     * Get files from a tree structure, according with
     * allowed extensions and exclude folders.
     *
     * @param \RecursiveDirectoryIterator $tree
     * @param array $allowedExtensions
     * @param array $excludeFolders
     * @return array
     * @author Lounis BENAMAR
     */
    final public static function getTreeFilesByFilters(\RecursiveDirectoryIterator $tree, array $allowedExtensions = null, array $excludeFolders = null) {
        $result = [];
        foreach($tree as $fileObject) {
            $fileName = $fileObject->getFileName();
            $filePath = $fileObject->getPathName();
            $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);

            if(!$fileExt) {
                continue;
            }

            if(isset($allowedExtensions) && !ArrayUtils::arrayContains($allowedExtensions, $fileExt)) {
                continue;
            }

            if(isset($excludeFolders) && !ArrayUtils::arrayContains($excludeFolders, $filePath)) {
                continue;
            }

            $result[] = $fileName;

        }

        return $result;
    }

    /** Obtain directories recursively from a base path.
     *
     * @param $baseDir
     * @return array
     * @author Lounis BENAMAR
     */
    final public static function getDirsRecursively($baseDir): array {
        $recursiveIteratorIterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(
                $baseDir,
                \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_SELF
            )
        );

        foreach($recursiveIteratorIterator as $item) {
            if ($item->isDir() && $item->getFileName() !== '..') {
                $items[] = substr($item->getPathname(),0, -2);
            }
        }

        return $items ?? [];
    }

}