<?php

/**
 * ArrayUtils provide methods
 * for array usage.
 */

namespace App\Utils\Core;

final class ArrayUtils {

    /**
     * Check if a list of values from an array contain
     * a specific pattern.
     *
     * @param array $list
     * @param string $pattern
     * @param bool|null $caseSensitive
     * @return bool
     * @author Lounis BENAMAR
     */
    final public static function arrayContains(array $list, string $pattern, bool $caseSensitive = null) {
        foreach ($list as $item) {
            if(isset($caseSensitive) || $caseSensitive) {
                if(strpos($item, $pattern) !== FALSE) {
                    return true;
                }
            } else {
                if(stripos($item, $pattern) !== FALSE) {
                    return true;
                }
            }
        }

        return false;
    }


}