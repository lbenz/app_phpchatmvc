<?php

namespace App\Utils;

class MemberUtils {

    /**
     * Get all sessions of users connected in the application
     *
     * @return array
     */
    public static function getAllUsersSession() {
        $sessionId = session_id(); // default session id as logged in user
        $sessions = [];
        $sessionNames = scandir(session_save_path()); // path session application storage
        foreach($sessionNames as $sessionName) {
            if(strpos($sessionName,".") === false) { // skips other files
                $sessionName = str_replace("sess_","",$sessionName); // get pure session id of file
                session_abort(); // close session and conserve original state
                session_id($sessionName); // load new session data
                session_start(); // start session to get SUPERGLOBAL $_SESSION synchronized
                $sessions[$sessionName] = $_SESSION; // save session in array
                // restore session to previous state
                session_abort();
                session_id($sessionId);
                session_start();
            }
        }

        return $sessions;
    }

    /**
     * Method to get only users connected on Box
     *
     * @return array
     */
    public static function getAllUsersConnected() {
        $members = array();
        $sessions = self::getAllUsersSession();
        foreach($sessions as $session) {
            if(isset($session['member'])) {
                if(($session['member']['timestamp'] + 10) > time()) {
                    $member = unserialize($session['member']['serialize']);
                    $members[$member->getUsername()] = [
                        'username' => $member->getUsername(),
                        'lastActivity' => $session['member']['timestamp']
                    ];
                }
            }
        }

        return $members;
    }
}