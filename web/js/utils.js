/**
 *
 * @param timestamp
 * @returns {string}
 */
function timestampToDateFr(timestamp) {
    var date = new Date(timestamp * 1000);
    return date.getDate() + '/' + formatMonth(date.getMonth()) + '/' + formatYear(date.getFullYear()) + ' ' +
        date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
}

/**
 *
 * @param month
 * @returns {string}
 */
function formatMonth(month) {
    var prefix = '';
    if(month < 10) {
        prefix = '0';
    }

    return prefix + (month + 1);
}

/**
 *
 * @param year
 * @returns {T[] | SharedArrayBuffer | BigUint64Array | Uint8ClampedArray | Uint32Array | Blob | Int16Array | Float64Array | Float32Array | string | Uint16Array | ArrayBuffer | Int32Array | BigInt64Array | Uint8Array | Int8Array | *}
 */
function formatYear(year) {
    return ('' + year).slice(2);
}