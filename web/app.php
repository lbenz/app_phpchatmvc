<?php

namespace App;

use App\Config\Bootstrap;

require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/../config/Autoloader.php';
require_once __DIR__ . '/../config/Bootstrap.php';
require_once __DIR__ . '/../vendor/autoload.php'; // composer autoload

Bootstrap::bootApp();
