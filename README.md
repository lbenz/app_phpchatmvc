# PHP-CHAT-MVC

PHP chatbox mvc project

## Installation

### Prerequisites

INSTALL DOCKER

```
apt-get install -y docker
apt-get install -y docker-compose
apt-get install -y docker-doc
```

### Install project

Get into root folder of whole project

Launch docker-compose environment

```
docker-compose up -d
```

URL/Host of this project is : php-chat-mvc.local / www.php-chat-mvc.local
(add this host to your OS, Windows or Linux hostfile)

PhpMyAdmin : http://localhost:8000

Check host file of your OS to synchronise it

Import with PhpMyAdmin the database php-chat-mvc.sql at root folder **httpdocs** from source code of project.

Get into source code **httpdocs** and install dependencies
```
composer install
```

## Features

* Login
* Register
* Generate members
* Get archives messages
* See users connected in real time
* Send and receive message in real time

## Running the tests

We've used PHPUNIT for test

Example :

```
vendor/bin/phpunit ./tests/Controllers/MemberControllerTest.php
```

## Built With

* [Docker](https://docs.docker.com/)
* [Composer](https://getcomposer.org/) - Library Management

## Authors

* **LOUNIS BENAMAR** -  [Project source code](https://bitbucket.org/lbenz/app_phpchatmvc)

## License

This project is licensed under the MIT License

## Other

* This project lead me to create a micro framework.
* SEMVER : 0.1.0